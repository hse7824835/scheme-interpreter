#pragma once

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <cstdint>

class Interpreter;

class Object : public std::enable_shared_from_this<Object> {
public:
    virtual ~Object() = default;
};

class Number : public Object {
public:
    Number(int value) : value_(value) {
    }
    int GetValue() const {
        return value_;
    }

private:
    int64_t value_;
};

class Symbol : public Object {
public:
    Symbol(std::string name) : name_(name) {
    }
    const std::string& GetName() const {
        return name_;
    }

private:
    std::string name_;
};

class Cell : public Object {
public:
    Cell(std::shared_ptr<Object> first, std::shared_ptr<Object> second)
        : first_(first), second_(second) {
    }
    std::shared_ptr<Object> GetFirst() const {
        return first_;
    }
    std::shared_ptr<Object> GetSecond() const {
        return second_;
    }
    std::shared_ptr<Object>& GetFirst() {
        return first_;
    }
    std::shared_ptr<Object>& GetSecond() {
        return second_;
    }

private:
    std::shared_ptr<Object> first_, second_;
};

class PrimitiveFunction : public Object {
public:
    PrimitiveFunction(std::shared_ptr<Object> (*function)(Interpreter*, std::shared_ptr<Object>))
        : function_(function) {
    }
    std::shared_ptr<Object> Run(Interpreter* interpreter, std::shared_ptr<Object> input) {
        return function_(interpreter, input);
    }

private:
    std::shared_ptr<Object> (*function_)(Interpreter*, std::shared_ptr<Object>);
};

class Lambda : public Object {
public:
    Lambda(std::vector<std::string>&& args, std::shared_ptr<Cell> eval,
           std::shared_ptr<Lambda> parent)
        : args_(std::move(args)), eval_(eval), parent_(parent) {
    }
    std::unordered_map<std::string, std::shared_ptr<Object>>& GetLocals() {
        return locals_;
    }
    std::vector<std::string>& GetArguments() {
        return args_;
    }
    std::shared_ptr<Cell>& GetEvaluateList() {
        return eval_;
    }
    std::shared_ptr<Lambda>& GetParent() {
        return parent_;
    }

private:
    std::unordered_map<std::string, std::shared_ptr<Object>> locals_;
    std::vector<std::string> args_;
    std::shared_ptr<Cell> eval_;
    std::shared_ptr<Lambda> parent_;
};

///////////////////////////////////////////////////////////////////////////////

// Runtime type checking and convertion.
// This can be helpful: https://en.cppreference.com/w/cpp/memory/shared_ptr/pointer_cast

template <class T>
std::shared_ptr<T> As(const std::shared_ptr<Object>& obj) {
    return std::dynamic_pointer_cast<T>(obj);
}

template <class T>
bool Is(const std::shared_ptr<Object>& obj) {
    return bool(std::dynamic_pointer_cast<T>(obj));
}

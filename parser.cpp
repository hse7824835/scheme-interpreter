#include <parser.h>

std::shared_ptr<Object> Read(Tokenizer *tokenizer) {
    if (tokenizer->IsEnd()) {
        throw SyntaxError("Empty input.");
    }
    Token token = tokenizer->GetToken();
    tokenizer->Next();
    if (SymbolToken *symbol_ptr = std::get_if<SymbolToken>(&token)) {
        return std::make_shared<Symbol>(symbol_ptr->name);
    } else if (std::get_if<QuoteToken>(&token)) {
        if (tokenizer->IsEnd()) {
            throw SyntaxError("<'> followed by nothing.");
        }
        return std::make_shared<Cell>(std::make_shared<Symbol>("quote"),
                                      std::make_shared<Cell>(Read(tokenizer), nullptr));
    } else if (std::get_if<DotToken>(&token)) {
        throw SyntaxError("<.> is not a value.");
    } else if (BracketToken *bracket_ptr = std::get_if<BracketToken>(&token)) {
        if (*bracket_ptr == BracketToken::OPEN) {
            return ReadList(tokenizer);
        } else {
            throw SyntaxError("<)> is not a value.");
        }
    } else if (ConstantToken *constant_ptr = std::get_if<ConstantToken>(&token)) {
        return std::make_shared<Number>(constant_ptr->value);
    }

    throw SyntaxError("Invalid token.");
}
std::shared_ptr<Object> ReadList(Tokenizer *tokenizer) {
    std::shared_ptr<Cell> root = nullptr, cur = nullptr;
    while (true) {
        if (tokenizer->IsEnd()) {
            throw SyntaxError("Unclosed bracket.");
        }
        Token token = tokenizer->GetToken();
        if (BracketToken *bracket_ptr = std::get_if<BracketToken>(&token)) {
            if (*bracket_ptr == BracketToken::CLOSE) {
                tokenizer->Next();
                break;
            }
        }
        if (std::get_if<DotToken>(&token)) {
            if (!root) {
                throw SyntaxError("<.> cannot be the first item in list.");
            }
            tokenizer->Next();
            if (tokenizer->IsEnd()) {
                throw SyntaxError("<.> cannot be the last token.");
            }
            cur->GetSecond() = Read(tokenizer);
            if (tokenizer->IsEnd()) {
                throw SyntaxError("Element after the <.> cannot be the last element.");
            }
            token = tokenizer->GetToken();
            if (BracketToken *bracket_ptr = std::get_if<BracketToken>(&token)) {
                if (*bracket_ptr == BracketToken::CLOSE) {
                    tokenizer->Next();
                    break;
                }
            }
            throw SyntaxError("<.> cannot be in the middle of a list.");
        }
        std::shared_ptr<Cell> cell = std::make_shared<Cell>(Read(tokenizer), nullptr);
        if (root) {
            cur->GetSecond() = cell;
        } else {
            root = cell;
        }
        cur = cell;
    }

    return root;
}

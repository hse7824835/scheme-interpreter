#include <tokenizer.h>

bool SymbolToken::operator==(const SymbolToken& other) const {
    return name == other.name;
}

bool QuoteToken::operator==(const QuoteToken&) const {
    return true;
}

bool DotToken::operator==(const DotToken&) const {
    return true;
}

bool ConstantToken::operator==(const ConstantToken& other) const {
    return value == other.value;
}

Tokenizer::Tokenizer(std::istream* in) : in_(in) {
    Append();
}

bool Tokenizer::IsEnd() {
    Append();
    return tokens_.empty();
}

void Tokenizer::Next() {
    Append();
    tokens_.pop();
}

Token Tokenizer::GetToken() {
    Append();
    return tokens_.front();
}

void Tokenizer::Append() {
    static const std::string kSymbolStartString =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>*/#";
    static const std::string kSymbolInString =
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>*/#0123456789?!-";
    static const std::unordered_set<int> kSymbolStartSet(kSymbolStartString.begin(),
                                                         kSymbolStartString.end());
    static const std::unordered_set<int> kSymbolInSet(kSymbolInString.begin(),
                                                      kSymbolInString.end());
    static const std::unordered_set<int> kNumStartSet{'0', '1', '2', '3', '4', '5',
                                                      '6', '7', '8', '9', '+', '-'};
    static const std::unordered_set<int> kNumInSet{'0', '1', '2', '3', '4',
                                                   '5', '6', '7', '8', '9'};
    int ch = in_->get();
    while (ch != EOF) {
        if (kNumStartSet.contains(ch)) {
            std::string word;
            word.push_back(ch);
            ch = in_->get();
            while (kNumInSet.contains(ch)) {
                word.push_back(ch);
                ch = in_->get();
            }
            if (word == "+" || word == "-") {
                tokens_.push(SymbolToken{word});
            } else {
                tokens_.push(ConstantToken{std::stoi(word)});
            }
        } else if (ch == '(') {
            tokens_.push(BracketToken::OPEN);
            ch = in_->get();
        } else if (ch == ')') {
            tokens_.push(BracketToken::CLOSE);
            ch = in_->get();
        } else if (ch == '\'') {
            tokens_.push(QuoteToken{});
            ch = in_->get();
        } else if (ch == '.') {
            tokens_.push(DotToken{});
            ch = in_->get();
        } else if (kSymbolStartSet.contains(ch)) {
            std::string word;
            word.push_back(ch);
            ch = in_->get();
            while (kSymbolInSet.contains(ch)) {
                word.push_back(ch);
                ch = in_->get();
            }
            tokens_.push(SymbolToken{word});
        } else {
            ch = in_->get();
        }
    }
    in_->clear();
    in_->seekg(0, in_->end);
}

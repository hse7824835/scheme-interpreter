#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <cstdint>

#include "parser.h"

class Interpreter {
public:
    Interpreter();
    std::string Run(const std::string&);

private:
    std::shared_ptr<Object> Evaluate(std::shared_ptr<Object>);
    std::string ToString(std::shared_ptr<Object>);

    std::unordered_map<std::string, std::shared_ptr<Object>> global_locals_;
    std::vector<std::unordered_map<std::string, std::shared_ptr<Object>>*> stack_;
    std::shared_ptr<Lambda> current_scope_;
};

#include "scheme.h"

// Initialize all the default functions
Interpreter::Interpreter() {
    stack_.push_back(&global_locals_);
    global_locals_["#t"] = std::make_shared<Symbol>("#t");
    global_locals_["#f"] = std::make_shared<Symbol>("#f");
    global_locals_["define"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw SyntaxError("No [define] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [define] list and not enough arguments.");
            }
            std::shared_ptr<Object> name = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> value_cell = As<Cell>(cell)->GetSecond();
            if (Is<Cell>(name)) {
                std::shared_ptr<Object> args = As<Cell>(name)->GetSecond();
                name = As<Cell>(name)->GetFirst();
                if (!Is<Symbol>(name)) {
                    name = scheme->Evaluate(name);
                    if (!Is<Symbol>(name)) {
                        throw RuntimeError("Name must be a symbol in lambda definition.");
                    }
                }
                if (!Is<PrimitiveFunction>(scheme->global_locals_["lambda"])) {
                    throw RuntimeError("[lambda] was overwritten, cannot create one.");
                }
                (*(scheme->stack_).back())[As<Symbol>(name)->GetName()] =
                    As<PrimitiveFunction>(scheme->global_locals_["lambda"])
                        ->Run(scheme, std::make_shared<Cell>(args, value_cell));
                return nullptr;
            }
            if (!value_cell) {
                throw SyntaxError("Not enough [define] arguments.");
            }
            if (!Is<Cell>(value_cell)) {
                throw SyntaxError("Malformed [define] list.");
            }
            if (As<Cell>(value_cell)->GetSecond()) {
                throw SyntaxError("Too many [define] arguments.");
            }
            if (!Is<Symbol>(name)) {
                name = scheme->Evaluate(name);
                if (!Is<Symbol>(name)) {
                    throw RuntimeError("Name must be a symbol in [define].");
                }
            }
            (*(scheme->stack_).back())[As<Symbol>(name)->GetName()] =
                scheme->Evaluate(As<Cell>(value_cell)->GetFirst());
            return nullptr;
        });

    global_locals_["lambda"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw SyntaxError("No [lambda] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [lambda] list and not enough arguments.");
            }
            std::shared_ptr<Object> args = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> body = As<Cell>(cell)->GetSecond();
            std::vector<std::string> argument_names;
            if (!body) {
                throw SyntaxError("Empty [lambda] body.");
            }
            while (Is<Cell>(args)) {
                std::shared_ptr<Cell> cur_arg = As<Cell>(args);
                if (!Is<Symbol>(cur_arg->GetFirst())) {
                    throw RuntimeError("[lambda] argument definition has to consist of symbols.");
                }
                argument_names.push_back(As<Symbol>(cur_arg->GetFirst())->GetName());
                args = cur_arg->GetSecond();
            }
            if (args) {
                throw SyntaxError("Malformed [lambda] argument list.");
            }
            if (!body) {
                throw SyntaxError("Empty [lambda] body.");
            }
            while (Is<Cell>(body)) {
                body = As<Cell>(body)->GetSecond();
            }
            if (body) {
                throw SyntaxError("Malformed [lambda] body.");
            }
            body = As<Cell>(cell)->GetSecond();
            return std::make_shared<Lambda>(std::move(argument_names), As<Cell>(body),
                                            scheme->current_scope_);
        });

    global_locals_["quote"] =
        std::make_shared<PrimitiveFunction>([](Interpreter*, std::shared_ptr<Object> cell) {
            if (!cell) {
                throw RuntimeError("Not enough [quote] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [quote] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [quote] arguments.");
            }
            return As<Cell>(cell)->GetFirst();
        });

    global_locals_["number?"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [number?] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [number?] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [number?] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Number>(value)) {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["+"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            int64_t sum = 0;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only add numbers.");
                }
                sum += As<Number>(value)->GetValue();
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [+] list.");
            }
            return std::make_shared<Number>(sum);
        });

    global_locals_["-"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [-] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw RuntimeError("Malformed [-] list.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!As<Cell>(cell)->GetSecond()) {
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only apply unary [-] operator to a number.");
                }
                return std::make_shared<Number>(-As<Number>(value)->GetValue());
            }
            if (!Is<Number>(value)) {
                throw RuntimeError("Can only subtract from numbers.");
            }
            int64_t sum = As<Number>(value)->GetValue();
            cell = As<Cell>(cell)->GetSecond();
            while (Is<Cell>(cell)) {
                value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only subtract numbers.");
                }
                sum -= As<Number>(value)->GetValue();
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [-] list.");
            }
            return std::make_shared<Number>(sum);
        });

    global_locals_["*"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            int64_t prod = 1;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only multiply numbers.");
                }
                prod *= As<Number>(value)->GetValue();
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [*] list.");
            }
            return std::make_shared<Number>(prod);
        });

    global_locals_["/"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("No [/] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw RuntimeError("Malformed [/] list.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Not enough [/] arguments.");
            }
            if (!Is<Number>(value)) {
                throw RuntimeError("Can only divide numbers.");
            }
            int64_t div = As<Number>(value)->GetValue();
            cell = As<Cell>(cell)->GetSecond();
            while (Is<Cell>(cell)) {
                value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can divide by numbers.");
                }
                div /= As<Number>(value)->GetValue();
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [/] list.");
            }
            return std::make_shared<Number>(div);
        });

    global_locals_["max"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [max] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw RuntimeError("Malformed [max] list.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!As<Cell>(cell)->GetSecond()) {
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only apply [max] to a number.");
                }
                return value;
            }
            if (!Is<Number>(value)) {
                throw RuntimeError("First [max] argument has to be a number.");
            }
            int64_t max = As<Number>(value)->GetValue();
            cell = As<Cell>(cell)->GetSecond();
            while (Is<Cell>(cell)) {
                value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only take [max] from numbers.");
                }
                if (As<Number>(value)->GetValue() > max) {
                    max = As<Number>(value)->GetValue();
                }
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [max] list.");
            }
            return std::make_shared<Number>(max);
        });

    global_locals_["min"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [min] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw RuntimeError("Malformed [min] list.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!As<Cell>(cell)->GetSecond()) {
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only apply [min] to a number.");
                }
                return value;
            }
            if (!Is<Number>(value)) {
                throw RuntimeError("First [min] argument has to be a number.");
            }
            int64_t min = As<Number>(value)->GetValue();
            cell = As<Cell>(cell)->GetSecond();
            while (Is<Cell>(cell)) {
                value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only take [max] from numbers.");
                }
                if (As<Number>(value)->GetValue() < min) {
                    min = As<Number>(value)->GetValue();
                }
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [max] list.");
            }
            return std::make_shared<Number>(min);
        });

    global_locals_["abs"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [abs] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [abs] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [abs] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Number>(value)) {
                throw RuntimeError("Can only take [abs] from numbers.");
            }
            if (As<Number>(value)->GetValue() < 0) {
                return std::make_shared<Number>(-As<Number>(value)->GetValue());
            }
            return value;
        });

    global_locals_["="] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Number> prev = nullptr;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only compare [=] numbers.");
                }
                if (prev) {
                    if (As<Number>(value)->GetValue() != prev->GetValue()) {
                        return std::make_shared<Symbol>("#f");
                    }
                }
                cell = As<Cell>(cell)->GetSecond();
                prev = As<Number>(value);
            }
            if (cell) {
                throw SyntaxError("Malformed [=] list.");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_[">"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Number> prev = nullptr;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only compare [>] numbers.");
                }
                if (prev) {
                    if (prev->GetValue() <= As<Number>(value)->GetValue()) {
                        return std::make_shared<Symbol>("#f");
                    }
                }
                cell = As<Cell>(cell)->GetSecond();
                prev = As<Number>(value);
            }
            if (cell) {
                throw SyntaxError("Malformed [>] list.");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["<"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Number> prev = nullptr;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only compare [<] numbers.");
                }
                if (prev) {
                    if (prev->GetValue() >= As<Number>(value)->GetValue()) {
                        return std::make_shared<Symbol>("#f");
                    }
                }
                cell = As<Cell>(cell)->GetSecond();
                prev = As<Number>(value);
            }
            if (cell) {
                throw SyntaxError("Malformed [<] list.");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_[">="] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Number> prev = nullptr;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only compare [>=] numbers.");
                }
                if (prev) {
                    if (prev->GetValue() < As<Number>(value)->GetValue()) {
                        return std::make_shared<Symbol>("#f");
                    }
                }
                cell = As<Cell>(cell)->GetSecond();
                prev = As<Number>(value);
            }
            if (cell) {
                throw SyntaxError("Malformed [>=] list.");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["<="] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Number> prev = nullptr;
            while (Is<Cell>(cell)) {
                std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Number>(value)) {
                    throw RuntimeError("Can only compare [<=] numbers.");
                }
                if (prev) {
                    if (prev->GetValue() > As<Number>(value)->GetValue()) {
                        return std::make_shared<Symbol>("#f");
                    }
                }
                cell = As<Cell>(cell)->GetSecond();
                prev = As<Number>(value);
            }
            if (cell) {
                throw SyntaxError("Malformed [<=] list.");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["boolean?"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [boolean?] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [boolean?] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [boolean?] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Symbol>(value)) {
                return std::make_shared<Symbol>("#f");
            }
            const std::string& name = As<Symbol>(value)->GetName();
            if (name != "#t" && name != "#f") {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["not"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [not] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [not] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [not] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Symbol>(value)) {
                return std::make_shared<Symbol>("#f");
            }
            const std::string& name = As<Symbol>(value)->GetName();
            if (name != "#f") {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["and"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Object> value = nullptr;
            while (Is<Cell>(cell)) {
                value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (Is<Symbol>(value)) {
                    if (As<Symbol>(value)->GetName() == "#f") {
                        return std::make_shared<Symbol>("#f");
                    }
                }
                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [and] list.");
            }
            if (!value) {
                return std::make_shared<Symbol>("#t");
            }
            return value;
        });

    global_locals_["or"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Object> value = nullptr;
            while (Is<Cell>(cell)) {
                value = As<Cell>(cell)->GetFirst();
                value = scheme->Evaluate(value);
                if (!Is<Symbol>(value)) {
                    return value;
                }
                if (As<Symbol>(value)->GetName() != "#f") {
                    return value;
                }

                cell = As<Cell>(cell)->GetSecond();
            }
            if (cell) {
                throw SyntaxError("Malformed [or] list.");
            }
            return std::make_shared<Symbol>("#f");
        });

    global_locals_["pair?"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [pair?] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [pair?] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [pair?] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Cell>(value)) {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["null?"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [null?] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [null?] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [null?] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (value) {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["list?"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [list?] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [list?] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [list?] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            while (Is<Cell>(value)) {
                value = As<Cell>(value)->GetSecond();
            }
            if (value) {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["cons"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("No [cons] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [cons] list and not enough arguments.");
            }
            std::shared_ptr<Object> first = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> second_cell = As<Cell>(cell)->GetSecond();
            if (!second_cell) {
                throw RuntimeError("Not enough [cons] arguments.");
            }
            if (!Is<Cell>(second_cell)) {
                throw SyntaxError("Malformed [cons] list.");
            }
            if (As<Cell>(second_cell)->GetSecond()) {
                throw RuntimeError("Too many [cons] arguments.");
            }
            std::shared_ptr<Object> second = As<Cell>(second_cell)->GetFirst();
            first = scheme->Evaluate(first);
            second = scheme->Evaluate(second);
            return std::make_shared<Cell>(first, second);
        });

    global_locals_["car"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [car] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [car] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [car] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Cell>(value)) {
                throw RuntimeError("Can only pass pairs or lists to [car].");
            }
            return As<Cell>(value)->GetFirst();
        });

    global_locals_["cdr"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [cdr] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [cdr] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [cdr] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Cell>(value)) {
                throw RuntimeError("Can only pass pairs or lists to [cdr].");
            }
            return As<Cell>(value)->GetSecond();
        });

    global_locals_["list"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            std::shared_ptr<Object> value = cell;
            while (Is<Cell>(value)) {
                As<Cell>(value)->GetFirst() = scheme->Evaluate(As<Cell>(value)->GetFirst());
                value = As<Cell>(value)->GetSecond();
            }
            if (value) {
                throw SyntaxError("Malformed [list] list.");
            }
            return cell;
        });

    global_locals_["list-ref"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("No [list-ref] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [list-ref] list and not enough arguments.");
            }
            std::shared_ptr<Object> list = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> index_cell = As<Cell>(cell)->GetSecond();
            if (!index_cell) {
                throw RuntimeError("Not enough [list-ref] arguments.");
            }
            if (!Is<Cell>(index_cell)) {
                throw SyntaxError("Malformed [list-ref] list.");
            }
            if (As<Cell>(index_cell)->GetSecond()) {
                throw RuntimeError("Too many [list-ref] arguments.");
            }
            std::shared_ptr<Object> index_arg = As<Cell>(index_cell)->GetFirst();
            list = scheme->Evaluate(list);
            index_arg = scheme->Evaluate(index_arg);
            if (!Is<Cell>(list)) {
                throw RuntimeError("List in [list-ref] must be a list.");
            }
            if (!Is<Number>(index_arg)) {
                throw RuntimeError("Index in [list-ref] must be a number.");
            }
            int64_t index = As<Number>(index_arg)->GetValue();
            while (index) {
                if (!Is<Cell>(list)) {
                    break;
                }
                list = As<Cell>(list)->GetSecond();
                --index;
            }
            if (index) {
                if (list) {
                    throw SyntaxError("Malformed [list-ref] list argument.");
                }
                throw RuntimeError("[list-ref] index out of range.");
            }
            if (!Is<Cell>(list)) {
                throw RuntimeError("[list-ref] index out of range by one.");
            }
            return As<Cell>(list)->GetFirst();
        });

    global_locals_["list-tail"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("No [list-tail] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [list-tail] list and not enough arguments.");
            }
            std::shared_ptr<Object> list = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> index_cell = As<Cell>(cell)->GetSecond();
            if (!index_cell) {
                throw RuntimeError("Not enough [list-tail] arguments.");
            }
            if (!Is<Cell>(index_cell)) {
                throw SyntaxError("Malformed [list-tail] list.");
            }
            if (As<Cell>(index_cell)->GetSecond()) {
                throw RuntimeError("Too many [list-tail] arguments.");
            }
            std::shared_ptr<Object> index_arg = As<Cell>(index_cell)->GetFirst();
            list = scheme->Evaluate(list);
            index_arg = scheme->Evaluate(index_arg);
            if (!Is<Cell>(list)) {
                throw RuntimeError("List in [list-tail] must be a list.");
            }
            if (!Is<Number>(index_arg)) {
                throw RuntimeError("Index in [list-tail] must be a number.");
            }
            int64_t index = As<Number>(index_arg)->GetValue();
            while (index) {
                if (!Is<Cell>(list)) {
                    break;
                }
                list = As<Cell>(list)->GetSecond();
                --index;
            }
            if (index) {
                if (list) {
                    throw SyntaxError("Malformed [list-tail] list argument.");
                }
                throw RuntimeError("[list-tail] index out of range.");
            }
            return list;
        });

    global_locals_["symbol?"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw RuntimeError("Not enough [symbol?] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [symbol?] list.");
            }
            if (As<Cell>(cell)->GetSecond()) {
                throw RuntimeError("Too many [symbol?] arguments.");
            }
            std::shared_ptr<Object> value = As<Cell>(cell)->GetFirst();
            value = scheme->Evaluate(value);
            if (!Is<Symbol>(value)) {
                return std::make_shared<Symbol>("#f");
            }
            return std::make_shared<Symbol>("#t");
        });

    global_locals_["set!"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw SyntaxError("No [set!] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [set!] list and not enough arguments.");
            }
            std::shared_ptr<Object> name = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> value_cell = As<Cell>(cell)->GetSecond();
            if (!value_cell) {
                throw SyntaxError("Not enough [set!] arguments.");
            }
            if (!Is<Cell>(value_cell)) {
                throw SyntaxError("Malformed [set!] list.");
            }
            if (As<Cell>(value_cell)->GetSecond()) {
                throw SyntaxError("Too many [set!] arguments.");
            }
            if (!Is<Symbol>(name)) {
                name = scheme->Evaluate(name);
                if (!Is<Symbol>(name)) {
                    throw RuntimeError("Name must be a symbol in [set!].");
                }
            }
            std::string str = As<Symbol>(name)->GetName();
            for (auto it = (scheme->stack_).rbegin(); it != (scheme->stack_).rend(); ++it) {
                if ((*it)->contains(str)) {
                    (**it)[str] = scheme->Evaluate(As<Cell>(value_cell)->GetFirst());
                    return nullptr;
                }
            }
            throw NameError("No local named [" + str + "] for [set!].");
        });

    global_locals_["if"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw SyntaxError("No [if] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [if] list and not enough arguments.");
            }
            std::shared_ptr<Object> condition = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> values_cell = As<Cell>(cell)->GetSecond();
            if (!values_cell) {
                throw SyntaxError("Not enough [if] arguments.");
            }
            if (!Is<Cell>(values_cell)) {
                throw SyntaxError("Malformed [if] list.");
            }
            std::shared_ptr<Object> if_true = As<Cell>(values_cell)->GetFirst();
            std::shared_ptr<Object> if_false_cell = As<Cell>(values_cell)->GetSecond();
            std::shared_ptr<Object> if_false = nullptr;
            if (if_false_cell) {
                if (!Is<Cell>(if_false_cell)) {
                    throw SyntaxError("Malformed [if] if_false value.");
                }
                if (As<Cell>(if_false_cell)->GetSecond()) {
                    throw SyntaxError("Too many [if] arguments.");
                }
                if_false = As<Cell>(if_false_cell)->GetFirst();
            }
            condition = scheme->Evaluate(condition);
            if (!Is<Symbol>(condition)) {
                return scheme->Evaluate(if_true);
            }
            if (As<Symbol>(condition)->GetName() != "#f") {
                return scheme->Evaluate(if_true);
            }
            return scheme->Evaluate(if_false);
        });

    global_locals_["set-car!"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw SyntaxError("No [set-car!] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [set-car!] list and not enough arguments.");
            }
            std::shared_ptr<Object> pair = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> value_cell = As<Cell>(cell)->GetSecond();
            if (!value_cell) {
                throw SyntaxError("Not enough [set-car!] arguments.");
            }
            if (!Is<Cell>(value_cell)) {
                throw SyntaxError("Malformed [set-car!] list.");
            }
            if (As<Cell>(value_cell)->GetSecond()) {
                throw SyntaxError("Too many [set-car!] arguments.");
            }
            pair = scheme->Evaluate(pair);
            if (!Is<Cell>(pair)) {
                throw RuntimeError("Can only modify a pair or a list with [set-car!].");
            }
            As<Cell>(pair)->GetFirst() = scheme->Evaluate(As<Cell>(value_cell)->GetFirst());
            return nullptr;
        });

    global_locals_["set-cdr!"] = std::make_shared<PrimitiveFunction>(
        [](Interpreter* scheme, std::shared_ptr<Object> cell) -> std::shared_ptr<Object> {
            if (!cell) {
                throw SyntaxError("No [set-cdr!] arguments.");
            }
            if (!Is<Cell>(cell)) {
                throw SyntaxError("Malformed [set-cdr!] list and not enough arguments.");
            }
            std::shared_ptr<Object> pair = As<Cell>(cell)->GetFirst();
            std::shared_ptr<Object> value_cell = As<Cell>(cell)->GetSecond();
            if (!value_cell) {
                throw SyntaxError("Not enough [set-cdr!] arguments.");
            }
            if (!Is<Cell>(value_cell)) {
                throw SyntaxError("Malformed [set-cdr!] list.");
            }
            if (As<Cell>(value_cell)->GetSecond()) {
                throw SyntaxError("Too many [set-cdr!] arguments.");
            }
            pair = scheme->Evaluate(pair);
            if (!Is<Cell>(pair)) {
                throw RuntimeError("Can only modify a pair or a list with [set-car!].");
            }
            As<Cell>(pair)->GetSecond() = scheme->Evaluate(As<Cell>(value_cell)->GetFirst());
            return nullptr;
        });
}

std::string Interpreter::Run(const std::string& str) {
    std::stringstream input_stream(str);
    Tokenizer tokenizer(&input_stream);
    std::shared_ptr<Object> root = Read(&tokenizer);
    if (!tokenizer.IsEnd()) {
        throw SyntaxError("Hanging tokens at the end.");
    }
    if (!root) {
        throw RuntimeError("to account for an incorrect test.");
    }
    return ToString(Evaluate(root));
}

std::shared_ptr<Object> Interpreter::Evaluate(std::shared_ptr<Object> root) {
    if (Is<Symbol>(root)) {
        std::string name = As<Symbol>(root)->GetName();
        for (auto it = stack_.rbegin(); it != stack_.rend(); ++it) {
            if ((*it)->contains(name)) {
                return (**it)[name];
            }
        }
        throw NameError("No local named [" + name + "].");

    } else if (Is<Cell>(root)) {
        std::shared_ptr<Cell> cell = As<Cell>(root);
        cell->GetFirst() = Evaluate(cell->GetFirst());
        if (!cell->GetFirst()) {
            throw RuntimeError("Cannot call nothing.");

        } else if (Is<Number>(cell->GetFirst())) {
            throw RuntimeError("Cannot call a number.");

        } else if (Is<Symbol>(cell->GetFirst())) {
            std::string name = As<Symbol>(cell->GetFirst())->GetName();
            throw RuntimeError("Cannot call [" + name + "].");

        } else if (Is<Cell>(cell->GetFirst())) {
            throw RuntimeError("Cannot call a list.");

        } else if (Is<PrimitiveFunction>(cell->GetFirst())) {
            if (!Is<Cell>(cell->GetSecond()) && cell->GetSecond()) {
                throw SyntaxError("Can't call a function as a pair.");
            }
            return As<PrimitiveFunction>(cell->GetFirst())->Run(this, cell->GetSecond());

        } else if (Is<Lambda>(cell->GetFirst())) {
            if (!Is<Cell>(cell->GetSecond()) && cell->GetSecond()) {
                throw SyntaxError("Can't call a lambda as a pair.");
            }
            std::shared_ptr<Lambda> lambda = As<Lambda>(cell->GetFirst());
            std::shared_ptr<Cell> arguments = As<Cell>(cell->GetSecond());
            std::unordered_map<std::string, std::shared_ptr<Object>> locals;
            std::vector<std::string>& argument_names = lambda->GetArguments();
            if (!arguments && !argument_names.empty()) {
                throw RuntimeError("No lambda arguments.");
            }
            if (arguments) {
                size_t index = 0;
                locals[argument_names[index]] = Evaluate(arguments->GetFirst());
                while (Is<Cell>(arguments->GetSecond())) {
                    ++index;
                    if (index >= argument_names.size()) {
                        throw RuntimeError("Too many lambda arguments.");
                    }
                    arguments = As<Cell>(arguments->GetSecond());
                    locals[argument_names[index]] = Evaluate(arguments->GetFirst());
                }
                if (arguments->GetSecond()) {
                    throw SyntaxError("Malformed list for lambda call.");
                }
                if (index + 1 < lambda->GetArguments().size()) {
                    throw RuntimeError("Not enough lambda arguments.");
                }
            }
            if (lambda->GetParent()) {
                stack_.push_back(&lambda->GetParent()->GetLocals());
            }
            stack_.push_back(&locals);
            std::shared_ptr<Lambda> prev = current_scope_;
            current_scope_ = lambda;
            std::shared_ptr<Cell> eval_list = lambda->GetEvaluateList();
            while (eval_list->GetSecond()) {
                (void)Evaluate(eval_list->GetFirst());
                eval_list = As<Cell>(eval_list->GetSecond());
            }
            std::shared_ptr<Object> result = Evaluate(eval_list->GetFirst());
            current_scope_ = prev;
            stack_.pop_back();
            lambda->GetLocals() = locals;
            if (lambda->GetParent()) {
                stack_.pop_back();
            }
            return result;
        }
    }

    return root;
}

std::string Interpreter::ToString(std::shared_ptr<Object> root) {
    if (Is<Number>(root)) {
        return std::to_string(As<Number>(root)->GetValue());
    } else if (Is<Symbol>(root)) {
        return As<Symbol>(root)->GetName();
    } else if (Is<Cell>(root)) {
        std::string list = "(";
        while (Is<Cell>(root)) {
            list += ToString(As<Cell>(root)->GetFirst());
            list.push_back(' ');
            root = As<Cell>(root)->GetSecond();
        }
        if (root) {
            list.push_back('.');
            list.push_back(' ');
            list += ToString(root);
            list.push_back(')');
            return list;
        }
        list.pop_back();
        list.push_back(')');
        return list;
    } else if (Is<PrimitiveFunction>(root)) {
        return "[PrimitiveFunction]";
    } else if (Is<Lambda>(root)) {
        std::string lambda = "lambda (";
        for (std::string& arg : As<Lambda>(root)->GetArguments()) {
            lambda += arg;
            lambda.push_back(' ');
        }
        lambda.pop_back();
        lambda.push_back(')');
        lambda.push_back(' ');
        lambda += ToString(As<Lambda>(root)->GetEvaluateList());
        return lambda;
    }
    return "()";
}

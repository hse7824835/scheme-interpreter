#include <iostream>
#include <string>

#include "scheme.h"


int main() {
    Interpreter interpreter;
    std::string in;
    for (;;) {
        getline(std::cin, in);
        try {
            std::cout << interpreter.Run(in) << std::endl;
        } catch (SyntaxError& error) {
            std::cout << "SyntaxError: " << error.what() << std::endl;
        } catch (RuntimeError& error) {
            std::cout << "RuntimeError: " << error.what() << std::endl;
        } catch (NameError& error) {
            std::cout << "NameError: " << error.what() << std::endl;
        }
    }
}